Name:           A-FOT
Version:        v1.0
Release:        4
Summary:        automatic feedback-directed optimization tool for openEuler
License:        MulanPSL-2.0
URL:            https://gitee.com/openeuler/A-FOT

Source0:        %{name}-%{version}.tar.gz

Buildrequires:  gcc-c++
Requires:       gcc gcc-c++ autofdo llvm-bolt Bear python3

Patch1: 0001-Add-Bear-to-A-FOT.patch
Patch2: 0002-Bugfix-Remove-Backslash-in-Options.patch
Patch3: 0003-PGO-kernel-Add-PGO-kernel-mode.patch
Patch4: 0004-Add-reboot-action-to-fasten-the-second-compilation.patch
Patch5: 0005-refactor-Reuse-calcsum-delay-pre-reboot-operations.patch

%description
A-FOT is an automatic feedback-directed optimization tool for openEuler

# skip debuginfo packages
%global debug_package %{nil}

%prep
%autosetup -n %{name}-%{version}

%build
g++ GcovSummaryAddTool.cpp -o calcsum

%install
mkdir -p %{buildroot}/%{_bindir}
cp a-fot %{buildroot}/%{_bindir}
cp a-fot.ini %{buildroot}/%{_bindir}
cp auto_fdo.sh %{buildroot}/%{_bindir}
cp auto_prefetch.sh %{buildroot}/%{_bindir}
cp auto_bolt.sh %{buildroot}/%{_bindir}
cp auto_kernel_pgo.sh %{buildroot}/%{_bindir}
cp split_json.py %{buildroot}/%{_bindir}
cp calcsum %{buildroot}/%{_bindir}

%files
%attr(755, root, root) %{_bindir}/a-fot
%attr(664, root, root) %{_bindir}/a-fot.ini
%attr(644, root, root) %{_bindir}/auto_fdo.sh
%attr(644, root, root) %{_bindir}/auto_prefetch.sh
%attr(644, root, root) %{_bindir}/auto_bolt.sh
%attr(644, root, root) %{_bindir}/auto_kernel_pgo.sh
%attr(644, root, root) %{_bindir}/split_json.py
%attr(755, root, root) %{_bindir}/calcsum
%license LICENSE
%doc README.md

%changelog
* Tue Jul 11 2023 Xiong Zhou <xiongzhou4@huawei.com> - v1.0-4
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/A-FOT
       Add reboot action to fasten the second compilation,
       fix bugs and format codes.
	   Reuse calcsum, delay pre-reboot operations and fixs typos.

* Fri Jun 2 2023 Xiong Zhou <xiongzhou4@huawei.com> - v1.0-3
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patch from openeuler/A-FOT
       Add PGO kernel mode.

* Mon Dec 19 2022 huitailangzju <804544223@qq.com> - v1.0-2
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Remove backslash in options

* Thu Dec 01 2022 huitailangzju <804544223@qq.com> - v1.0-1
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Add Bear to A-FOT

* Wed Mar 23 2022 liyancheng <412998149@qq.com> - 0
- Type:Init
- ID:NA
- SUG:NA
- DESC:Add automatic feedback optimization tool
